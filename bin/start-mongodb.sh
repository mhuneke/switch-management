#!/bin/bash

PROJECT_HOME=$( cd "$( dirname "$0" )/.." && pwd )

mongod run \
	--config "$PROJECT_HOME/bin/start-mongodb.conf" \
	--dbpath "$PROJECT_HOME/storage/mongodb" \
	--logpath "$PROJECT_HOME/logs/mongodb.log" \
	--logappend \
	--bind_ip 127.0.0.1
