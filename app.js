#!/usr/bin/env node
/*jshint node:true*/
'use strict';

/**
 * Module dependencies.
 */

/* TODO LIST:
  - Form button submit
  - Button builder
*/
var express = require('express'),
    routes = require('./routes'),
    RedisStore = require('connect-redis')(express),
    user = require('./routes/api/user'),
    widget = require('./routes/api/widget'),
    analytics = require('./routes/api/analytics'),
    documents = require('./routes/api/documents'),
    mailchimp = require('./routes/api/mailchimp'),
    auth = require('./routes/auth'),
    http = require('http'),
    path = require('path'),
    mongoose = require('mongoose'),
    fs = require('fs'),
    knox = require('knox'),
    redis = require('redis'),
    url = require('url'),
    config = require('./config'),
    stripe = require('stripe'),
    validatorbinding = require('./util/validatorbinding');

var app = express();

var redisClient;
if (process.env.REDISTOGO_URL) {
  var rtg = url.parse(process.env.REDISTOGO_URL);
  redisClient = redis.createClient(rtg.port, rtg.hostname);
  redisClient.auth(rtg.auth.split(':')[1]);
  exports.redisClient = redisClient;
} else {
  exports.redisClient = redisClient = redis.createClient();
}

// TODO : mongodb for session management
app.configure(function() {
  app.set('port', process.env.PORT || 3000);
  app.set('db', process.env.MONGOLAB_URI || 'mongodb://localhost/switch');
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session({
    secret: process.env.CLIENT_SECRET || "super secret string",
    maxAge: new Date(Date.now()) + 7200000,
    store: new RedisStore({client: redisClient})
  }));
  app.use(app.router);
  app.use(express['static'](path.join(__dirname, 'public')));
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost/switch');

app.get('/', routes.index);
app.get('/about', routes.about);
app.get('/contact', routes.contact);
app.get('/privacy', routes.privacy);
app.get('/terms', routes.terms);

// --- USER ---
app.get('/logout', auth.logout);
app.get('/login', auth.loginview);
app.post('/login', auth.login);
app.get('/register', auth.registerview);
app.post('/register', auth.register);

app.get('/app', auth.checkAuthentication, routes.appindex);

// --- DOCUMENT VIEWER ---
app.get('/documents/:id', documents.view);

// --- STRIPE WEBHOOK ---
app.get('/charge/callback', widget.chargecallback);

/*
app.get('/api/users', auth.checkAPIAuthentication, user.list);
app.post('/api/users', auth.checkAPIAuthentication, user.create);
app.put('/api/users/:id', auth.checkAPIAuthentication, user.update);
app.del('/api/users/:id', auth.checkAPIAuthentication, user.del);
*/

// --- API ---
app.get('/api/dashboard', auth.checkAPIAuthentication, user.dashboard);

app.get('/api/documents', documents.list);
app.get('/api/documents/:id', documents.download);
app.post('/api/documents', documents.create);

app.get('/api/mailchimp/lists', mailchimp.lists);
app.get('/api/mailchimp/lists/:id/groupings', mailchimp.listGroupings);

app.get('/api/widgets', auth.checkAPIAuthentication, widget.list);
app.get('/api/widgets/:id', auth.checkAPIAuthentication, widget.get);
app.post('/api/widgets', auth.checkAPIAuthentication, widget.create);
app.put('/api/widgets/:id', auth.checkAPIAuthentication, widget.update);
app.del('/api/widgets/:id', auth.checkAPIAuthentication, widget.del);

app.get('/api/widgets/:id/events/create', auth.checkAPIAuthentication, analytics.create);  // /create has to be added for jsonp
app.get('/api/widgets/:id/events', auth.checkAPIAuthentication, analytics.list);
app.get('/api/widgets/:id/charge', auth.checkAPIAuthentication, widget.charge);

app.get('/api/me', auth.checkAPIAuthentication, user.get);
app.put('/api/me', auth.checkAPIAuthentication, user.updateCurrentUser);

// --- EMBED ---
app.get('/api/widgets/:id/embed/js', widget.embedjs);
app.get('/api/widgets/:id/embed/css', widget.embedcss);
app.get('/api/widgets/:id/embed/html', widget.embedhtml);

exports.mongoose = mongoose;
exports.s3Client = knox.createClient({
    key: config.AWS_API_KEY,
    secret: config.AWS_SECRET_KEY,
    bucket: config.S3_BUCKET
});
exports.API_PAGE_SIZE = config.API_PAGE_SIZE;

var stripe = require('stripe')(config.STRIPE_SECRET_KEY);
exports.stripe = stripe;

app.listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
