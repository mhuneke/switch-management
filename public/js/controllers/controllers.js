(function(angular, _, $, prettyPrint, console, qq) {
'use strict';

var controllers = angular.module('switchapp.controllers', ['ngCookies']);

controllers.controller('AppController', ['$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
  $rootScope.$on("$routeChangeStart", function (event, next, current) {
    $scope.alertType = "";
    $scope.alertMessage = "Loading...";
    $scope.active = "progress-striped active progress-warning";
  });
  $rootScope.$on("$routeChangeSuccess", function (event, current, previous) {
    $scope.alertMessage = "";
    $scope.active = "";
    $scope.newLocation = $location.path();
  });
  $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
    $scope.alertType = "alert-error";
    $scope.alertMessage = "Failed to change routes :(";
    $scope.active = "";
  });
}]);

controllers.controller('HomeController', ['$scope', '$log', '$http', '$cookies', 'widgets', 'dashboard', function($scope, $log, $http, $cookies, widgets, dashboard) {
  $scope.widgets = widgets;
  $scope.dashboard = dashboard;
  $scope.viewFilter = $cookies.viewFilter || 'totalMoneyRaised';
  $scope.viewDirection = $cookies.viewDirection || 'desc';

  $scope.changeViewFilter = function(filter) {
    if ($scope.viewFilter !== filter) {
      $scope.viewFilter = filter;
      $cookies.viewFilter = filter;
      $scope.refreshWidgets();
    }
  }

  $scope.changeViewDirection = function(direction) {
    if ($scope.viewDirection !== direction) {
      $scope.viewDirection = direction;
      $cookies.viewDirection = direction;
      $scope.refreshWidgets();
    }
  }

  $scope.refreshWidgets = function() {
    $http.get('/api/widgets/?sort=' + $scope.viewFilter + '&direction=' + $scope.viewDirection).success(function(result) {
      $scope.widgets = result.data;
    });
  }
}]);

controllers.controller('DocumentsController', ['$scope', '$log', 'DocumentsService', 'documents', function($scope, $log, DocumentsService, documents) {
  $scope.documents = documents;

  var uploader = new qq.FileUploader({
    element: document.getElementById('file-uploader'),
    action: '/api/documents',
    sizeLimit: 26214400,
    debug: true,
    allowedExtensions: ['pdf'],
    onComplete: function(id, filename, responseJSON) {
      $scope.$apply(function() {
        DocumentsService.$get({}, function(result) {
          $scope.documents = result.data;
        });
      });
    }
  });
}]);

controllers.controller('ViewWidgetController', ['$scope', '$log', '$http', '$location', 'widgetData', function($scope, $log, $http, $location, widgetData) {
  $scope.widgetData = widgetData;

  $scope.deleteWidget = function() {
    $("#delete-widget").button('loading');
    $http.delete('/api/widgets/' + widgetData.widget._id).success(function(data) {
      $("#delete-widget").button('reset');
      $('#myModal').modal('hide');
      $location.url("/");
    });
  };
}]);

controllers.controller('AccountSettingsController', ['$scope', '$log', '$http', '$location', 'me', function($scope, $log, $http, $location, me) {
  $scope.me = me;

  $scope.save = function() {
    $http.put('/api/me', $scope.me).success(function(data) {
      $location.url("/");
    });
  };
}]);

controllers.controller('ViewDocumentController', ['$scope', '$log', 'doc', function($scope, $log, doc) {
  $scope.doc = doc;
}]);

controllers.controller('ConfigureAnalyticsWidgetController', ['$scope', '$log', '$http', '$location', 'widgetData', function($scope, $log, $http, $location, widgetData) {
  $scope.widgetData = widgetData;
}]);

controllers.controller('ConfigureMailchimpWidgetController', ['$scope', '$log', '$http', '$location', 'widgetData', 'mailchimpLists', function($scope, $log, $http, $location, widgetData, mailchimpLists) {
  $scope.widgetData = widgetData;
  $scope.mailchimpLists = mailchimpLists;

  $("#mailchimpList").val(widgetData.widget.subscribeMailchimpId);

  $scope.update = function() {
    var list = _.find($scope.mailchimpLists, function(list){ return list.id === widgetData.widget.subscribeMailchimpId });
    $http.put('/api/widgets/' + widgetData.widget._id, {subscribeMailchimpId: list.id, subscribeMailchimpName: list.name}).success(function(data) {
      $location.url("/widgets/" + widgetData.widget._id);
    });
  };
}]);

controllers.controller('CreateWidgetController', ['$scope', '$log', '$http', 'WidgetService', function($scope, $log, $http, WidgetService) {
  $scope.button = WidgetService;
  $scope.button.size = 'medium';
  $scope.button.color = 'gray';
  $scope.button.formbody = 'payment';

  $scope.maxButtonHeight = 300;

  $scope.getButtonHeight = function() {
    var currentHeightPercentage = $("#slider").slider("option", "value") / 100;
    return parseInt($scope.maxButtonHeight * currentHeightPercentage);
  };

  $scope.updateOwnText = function() {
    $scope.button.title = $('#own-text').val();
  };

  $scope.getFormBody = function() {
    if ($scope.button.formbody === 'payment') {
      return $('#payment-form').html();
    } else if ($scope.button.formbody === 'contact') {
      return $('#contact-form').html();
    }
  };

  $('#example').popover();
  prettyPrint();

  var isCompleted = false;

  $scope.currentStepIndex = 0;
  $scope.totalSteps = 5;

  $scope.previewButtonClass = function() {
    var buttonclass = '';

    if ($scope.button.size === 'large') {
      buttonclass = 'btn-large';
    } else if ($scope.button.size === 'small') {
      buttonclass = 'btn-small';
    }

    if ($scope.button.color === 'blue') {
      buttonclass += ' btn-primary';
    } else if ($scope.button.color === 'lightblue') {
      buttonclass += ' btn-info';
    } else if ($scope.button.color === 'green') {
      buttonclass += ' btn-success';
    } else if ($scope.button.color === 'yellow') {
      buttonclass += ' btn-warning';
    } else if ($scope.button.color === 'red') {
      buttonclass += ' btn-danger';
    }

    return buttonclass;
  };

  $scope.selectFormBody = function(formbody) {
     $scope.button.formbody = formbody;
  };

  $scope.selectColor = function(color) {
    $scope.button.color = color;
  };

  $scope.enterOption = function(test) {
    $("#option-" + test).addClass("selected");
  };

  $scope.leaveOption = function(test) {
    if ($("#option-" + test).attr("data-selected") !== "true") {
      $("#option-" + test).removeClass("selected");
    }
  };

  $scope.selectOwnText = function() {
    $scope.button.title = $('#own-text').val();
  };

  $scope.setButtonSize = function(size) {
    $scope.button.size = size;
  };

  $scope.setButtonTitle = function(title) {
    $scope.button.title = title;
  };

  $scope.next = function() {
    $scope.currentStepIndex++;
    $(".bar").css("width",  ($scope.currentStepIndex / $scope.totalSteps) * 100 +  "%");
  };

  $scope.finish = function() {
    $("#previewButton").removeAttr("rel").removeClass("ng-binding").removeAttr("data-original-title").removeAttr("popover").removeAttr("data-ng-class");

    var saveSuccess = function(data) {
      $scope.createdWidgetId = data._id;
      $scope.currentStepIndex++;
      $(".bar").css("width",  ($scope.currentStepIndex / $scope.totalSteps) * 100 +  "%");
    };
    var saveError = function(data) {
      console.log('save error:' + data);
    };
    $scope.button.buttonBody = $('.preview-button').html();
    $scope.button.formBody = $scope.getFormBody();
    $scope.button.$save(saveSuccess, saveError);
  };

  $scope.back = function () {
    $scope.currentStepIndex--;
    $(".bar").css("width",  ($scope.currentStepIndex / $scope.totalSteps) * 100 +  "%");
  };
}]);

}(angular, _, $, prettyPrint, console, qq));
