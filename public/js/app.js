(function(angular) {
'use strict';

var switchapp = angular.module('switchapp', ['switchapp.controllers', 'switchapp.services', 'switchapp.components', 'ui', 'ngCookies']);

switchapp.config(['$routeProvider', '$locationProvider', function($routes, $location) {

  $routes.when('/', {
    templateUrl: '/partials/home.html',
    controller: 'HomeController',
    resolve: {
      widgets: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/widgets/').success(function(result) {
          deferred.resolve(result.data);
        });
        return deferred.promise;
      },
      dashboard: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/dashboard').success(function(data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }
    }
  });

  $routes.when('/account', {
    templateUrl: '/partials/account-settings.html',
    controller: 'AccountSettingsController',
    resolve: {
      me: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/me').success(function(data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }
    }
  });

  $routes.when('/documents', {
    templateUrl: '/partials/documents.html',
    controller: 'DocumentsController',
    resolve: {
      documents: function($q, $route, DocumentsService) {
        var deferred = $q.defer();
        DocumentsService.$get({}, function(result) {
          deferred.resolve(result.data);
        });
        return deferred.promise;
      }
    }
  });



  $routes.when('/documents/:id', {
    templateUrl: '/partials/view-document.html',
    controller: 'ViewDocumentController',
    resolve: {
      doc: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/documents/' + $route.current.params.id).success(function(data) {
          if (data) {
            deferred.resolve(data);
          } else {
            deferred.reject("No document found.");
          }
        }).error(function(data) {
          deferred.reject("No document found.");
        });
        return deferred.promise;
      }
    }
  });

  $routes.when("/widgets/create", {
    templateUrl: "/partials/create-widget.html",
    controller: 'CreateWidgetController'
  });

  $routes.when('/widgets/:id', {
    templateUrl: '/partials/view-widget.html',
    controller: 'ViewWidgetController',
    resolve: {
      widgetData: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/widgets/' + $route.current.params.id).success(function(data) {
          if (data) {
            deferred.resolve(data);
          } else {
            deferred.reject("No widget found.");
          }
        }).error(function(data) {
          deferred.reject("No widget found.");
        });
        return deferred.promise;
      }
    }
  });

  $routes.when('/widgets/:id/analytics', {
    templateUrl: '/partials/configure-analytics-widget.html',
    controller: 'ConfigureAnalyticsWidgetController',
    resolve: {
      widgetData: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/widgets/' + $route.current.params.id).success(function(data) {
          if (data) {
            deferred.resolve(data);
          } else {
            deferred.reject("No widget found.");
          }
        }).error(function(data) {
          deferred.reject("No widget found.");
        });
        return deferred.promise;
      }
    }
  });

  $routes.when('/widgets/:id/mailchimp', {
    templateUrl: '/partials/configure-mailchimp-widget.html',
    controller: 'ConfigureMailchimpWidgetController',
    resolve: {
      widgetData: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/widgets/' + $route.current.params.id).success(function(data) {
          if (data) {
            deferred.resolve(data);
          } else {
            deferred.reject("No widget found.");
          }
        }).error(function(data) {
          deferred.reject("No widget found.");
        });
        return deferred.promise;
      },
      mailchimpLists: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/mailchimp/lists').success(function(data) {
          if (data) {
            deferred.resolve(data.data);
          } else {
            deferred.resolve({});
          }
        });
        return deferred.promise;
      }
    }
  });

  $routes.when('/widgets/:id/edit', {
    templateUrl: '/partials/edit-widget.html',
    controller: 'EditWidgetController',
    resolve: {
      widget: function($q, $route, $http) {
        var deferred = $q.defer();
        $http.get('/api/widgets/' + $route.current.params.id).success(function(data) {
          if (data) {
            deferred.resolve(data);
          } else {
            deferred.reject("No widget found.");
          }
        }).error(function(data) {
          deferred.reject("No widget found.");
        });
        return deferred.promise;
      }
    }
  });

}]);

}(angular));
