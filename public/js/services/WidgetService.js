(function(angular, _) {
'use strict';

var services = angular.module('switchapp.services', ['ngResource']);

services.factory('WidgetService', function($resource) {
  var WidgetResource = $resource('/api/widgets');
  return new WidgetResource();
});

services.factory('DocumentsService', function($resource) {
  var DocumentsResource = $resource('/api/documents');
  return new DocumentsResource();
});

}(angular, _));
