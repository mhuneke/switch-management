
/*
 * GET home page.
 */

var User = require('../model/user');

exports.index = function(req, res) {
  res.render('static/index');
};

exports.about = function(req, res) {
  res.render('static/about');
};

exports.contact = function(req, res) {
  res.render('static/contact');
};

exports.privacy = function(req, res) {
  res.render('static/privacy');
};

exports.terms = function(req, res) {
  res.render('static/terms');
};

exports.appindex = function(req, res) {
  if (!req.session.currentUser) {
    req.session.forward = req.originalUrl;
    res.redirect('/login');
  } else {
    res.render('index', { user: req.session.currentUser });
  }
  
};
