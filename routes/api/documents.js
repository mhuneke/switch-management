// curl -v -X GET http://localhost:3000/api/documents

var async = require('async'),
    Document = require('../../model/document'),
    app = require('../../app'),
    url = require('url'),
    crypto = require('crypto');

exports.view = function(req, res) {
  Document.findById(req.params.id, function(err, doc) {
    res.render('pdf/viewer', {doc: doc});
  });
};

// TODO: load these based on the current logged in user
exports.list = function(req, res) {
  var skip = req.query.skip || 0;
  var limit = app.API_PAGE_SIZE;
  // TODO: load these based on the current logged in user
  //var user = req.session.currentUser.id;

  async.waterfall([
    function(callback) {
      Document.find({user: req.session.currentUser._id}).skip(skip).limit(limit).exec(callback);
    },
    function(documents, callback) {
      Document.count(function(err, total) {
        callback(documents, total);
      });
    }
  ], function(documents, total) {
    var result = {};
    result.data = documents;
    result.page = (skip / limit) + 1;
    result.perpage = limit;
    result.total = total;
    res.json(result);
  });
};

exports.create = function(req, res) {
  var filename = '/' + req.session.currentUser._id + '/' + req.query.qqfile;
  var filesize = parseInt(req.headers["content-length"], 10);

  if(filesize > app.MAX_FILE_SIZE) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('{success:false, reason: "File too large" }');
    return;
  }

  var headers = {
      'Content-Length': req.headers['content-length'],
      'Content-Type': req.headers['content-type']
  };

  var file = app.s3Client.putStream(req, filename, headers, function(err, resp) {
    if(err) {
      res.json(500, {success:false, reason: err});
    } else {
      Document.create({user: req.session.currentUser._id, name: filename, path: file.url}, function(err) {
        if(err) {
          res.json(500, {success:false, reason: err});
        } else {
          res.json(200, {success: true});
        }
      });
    }
  });
};

exports.download = function(req, res) {
  Document.findById(req.params.id, function(err, doc) {
    app.s3Client.getFile(url.parse(doc.path).path, function(err, response) {
      var path = url.parse(doc.path).path;
      if (response.statusCode !== 200) {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('{success:false}');
      } else {
        var downloadFilename = path.substring(path.lastIndexOf('/') + 1, url.parse(doc.path).path.length);
        res.writeHead(200, {'Content-Type': 'application/file', 'Content-Length': response.headers['content-length'], 'Content-Disposition': 'inline; filename="' + downloadFilename + '"'});
        response.on('data', function(data) { res.write(data); });
        response.on('end', function(chunk) { res.end(); });
      }
    });
  });
};
