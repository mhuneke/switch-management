/*
 * GET widgets listing.
 */

// curl -v -H "Content-Type: application/json" -X GET -d '{}' http://localhost:3000/api/widgets
// curl -v -H "Content-Type: application/json" -X POST -d '{"user": "505fe1cf3cc5f43fc3000001", "projectName": "test", "title": "Donate", "width": "300", "height": "100", "webColorCode": "#777", "buttonBody": "<a class=\"switch-button btn\" href=\"#\">Donate</a>"}' http://localhost:3000/api/widgets
// curl -v -H "Content-Type: application/json" -X PUT -d '{"user": {"username": "mhuneke18@gmail.com", "password": "password"}}' http://localhost:3000/api/users/505d2ab95996e041e3000001
// curl -v -H "Content-Type: application/json" -X DELETE -d '{}' http://localhost:3000/api/users/505d2ab95996e041e3000001
// curl -v -H "Content-Type: application/json" -X GET -d '{}' http://localhost:3000/api/widgets/506083793b4d9adb74000001/charge?token=2

/*
<div id="switch-widget-7736" class="switch-widget"></div>
<script type="text/javascript" src="http://localhost:3000/api/widgets/:id/embed/js"></script>
*/

var async = require('async'),
    Widget = require('../../model/widget'),
    Client = require('../../model/client'),
    WidgetEvent = require('../../model/widget-event'),
    WidgetCharge = require('../../model/widget-charge'),
    User = require('../../model/user'),
    app = require('../../app'),
    config = require('../../config'),
    mailchimp = require('../api/mailchimp'),
    crypto = require('crypto'),
    _ = require('underscore'),
    fs = require('fs');

// TODO: load these based on the current logged in user
exports.list = function(req, res) {
  var skip = req.query.skip || 0;
  var limit = app.API_PAGE_SIZE;
  var sort = req.query.sort || 'totalMoneyRaised';
  var direction = req.query.direction || 'desc';
  var prefix = '';
  if (direction === 'desc') {
    prefix = '-';
  }
  // TODO: load these based on the current logged in user
  //var user = req.session.currentUser.id;

  async.waterfall([
    function(callback) {
      Widget.find({user: req.session.currentUser._id}).skip(skip).limit(limit).sort(prefix + sort).exec(callback);
    },
    function(widgets, callback) {
      Widget.count({user: req.session.currentUser._id}, function(err, total) {
        callback(widgets, total);
      });
    }
  ], function(widgets, total) {
    var result = {};
    result.data = widgets;
    result.page = (skip / limit) + 1;
    result.perpage = limit;
    result.total = total;
    res.json(result);
  });
};

exports.get = function(req, res) {
  var widgetWrapper = {};

  async.waterfall([
    function(callback) {
      Widget.findById(req.params.id, callback);
    }, function(widget, callback) {
      widgetWrapper.widget = widget;
      WidgetCharge.find({widget: widget._id}, 'amount', function(err, charges) {
        widgetWrapper.totalCharges = charges.length;
        var conversionPercentage = widget.viewCount == 0 ? 0 : (charges.length / widget.viewCount) * 100;
        widgetWrapper.conversionPercentage = Math.round(conversionPercentage * Math.pow(10,2)) / Math.pow(10,2); // to 2 decimal places
        callback(err);
      });
    }
  ], function(err, widget) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, widgetWrapper);
    }
  });
};

exports.create = function(req, res) {
  // TODO: hook up current user
  //if (req.body.widget) {
  //  req.body.widget.user = req.session.currentUser.id;
  //}

  // TODO: remove this
  if (!req.body.user) {
    req.body.user = req.session.currentUser._id;
  }

  Widget.create(req.body, function(err, widget) {
    if (err) {
      console.log(err);
      res.json(500, err);
    } else {
      res.json(200, widget);
    }
  });
};

exports.update = function(req, res) {
  Widget.findByIdAndUpdate(req.params.id, req.body, function(err, widget) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, {"success": "true"});
    }
  });
};

exports.del = function(req, res) {
  Widget.findByIdAndRemove(req.params.id, function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, {"success": "true"});
    }
  });
};

var createAnalyticsEvent = function(req, widget, client, type) {
  console.log(req.headers);
  var widgetEvent = {
    widget: widget._id,
    type: type,
    client: client._id,
    userAgent: req.headers['user-agent'],
    referer: req.headers['referer'],
    remoteAddress: req.headers['HTTP_X_FORWARDED'] || req.connection.remoteAddress
  };
  WidgetEvent.create(widgetEvent, function() {});
};

var loadWidgetAndCreateAnalyticsEvent = function(req, type, callback) {
  Widget.findById(req.params.id, function(err, widget) {
    var clientId = req.cookies.switchuser || new Buffer(crypto.randomBytes(32)).toString('base64');
    Client.findOne({username: clientId}, function(err, client) {
      if (err || client === null) {
        Client.create({username: clientId}, function(err, client) {
          createAnalyticsEvent(req, widget, client, type);
          callback(err, client, widget);
        });
      } else {
        createAnalyticsEvent(req, widget, client, type);
        callback(err, client, widget);
      }
    });
  });
};

exports.charge = function(req, res) {
  loadWidgetAndCreateAnalyticsEvent(req, 'charge', function(err, client, widget) {

    var stripeRequest = {
      amount: req.query.amount,
      currency: 'usd',
      card: req.query.card
    };

    // TODO: set this up
    // var id = "1234";
    // app.redisClient.hmset(id, wrapper);
    // app.redisClient.rpush('queue:switch', id);

    app.stripe.charges.create(stripeRequest,
      function chargeComplete(err, resp) {
        if (err) {
          console.log(err);
          console.log("something went wrong in chargeComplete");
          return;
        }
        console.log(resp);
        if (resp.paid) {
          var amountInDollars = resp.amount / 100;
          var feeInDollars = resp.fee / 100;
          var switchFeeInDollars = ((((resp.amount - resp.fee) * config.switchTransactionFeePercentage) - config.switchTransactionFee) / 100).toFixed(2);
          console.log("fee on " + amountInDollars + " = " + feeInDollars + " and " + switchFeeInDollars);
          var amountOwedInDollars = amountInDollars - feeInDollars - switchFeeInDollars;

          // Update viewcount, amount raised, add amount owed to user
          var widgetCharge = {
            widget: widget._id,
            client: client._id,
            amount: amountInDollars,
            fee: feeInDollars + switchFeeInDollars,
            feeDetails: res.fee_details,
            externalTransactionId: resp.id,
          };
          WidgetCharge.create(widgetCharge, function(err, widgetCharge) {});
          Widget.findByIdAndUpdate(widget._id, {totalMoneyRaised: widget.totalMoneyRaised + amountInDollars, donorCount: widget.donorCount + 1}, {}, function() {});
          User.findById(widget.user, function(err, user) {
            console.log("Adding " + amountOwedInDollars + " to " + user.amountOwed);
            User.findByIdAndUpdate(user._id, {amountOwed: user.amountOwed + amountOwedInDollars}, function() {});

            // add to mailchimp
            if (user.mailchimpAPIKey && widget.subscribeMailchimpId && req.paramXss('email')) {
              mailchimp.subscribeToList(user.mailchimpAPIKey, 
                widget.subscribeMailchimpId, req.paramXss('email'), widget.subscribeMailchimpGroupingId);
            }
          });
        }
    });

  });
  res.json(200, {"success": "true"});
};

exports.chargecallback = function(req, res) {
  var response = JSON.parse(req.body);

  // var widgetCharge = {
  //   widget: description.widget,
  //   client: description.client,
  //   amount: JSON.stringify(response)
  // };
  // WidgetCharge.create(widgetCharge, function(err, widgetCharge) {
  //   app.redisClient.rpush('queue:charge:webhook', widgetCharge._id);
  // });
  res.json(200, {success: "true"});
};

exports.embedjs = function(req, res) {
  res.setHeader('Content-Type', 'text/javascript; charset=UTF-8');
  loadWidgetAndCreateAnalyticsEvent(req, 'loaded', function(err, client, widget) {
    Widget.findByIdAndUpdate(widget._id, {viewCount: widget.viewCount + 1}, {}, function() {});
    res.cookie('switchuser', client.username, {expires: new Date(Date.now()+ app.COOKIE_EXPIRE_TIME), path: '/'});
    res.render('embed/widget-js', { baseUrl: config.BASE_URL, widget: widget });
  });
};

exports.embedcss = function(req, res) {
  res.setHeader('Content-Type', 'text/css; charset=UTF-8');
  Widget.findById(req.params.id, function(err, widget) {
    if (err || widget === null) {
      res.json(404, "{error: not found}");
    } else {
      res.render('embed/widget-css', { baseUrl: config.BASE_URL, widget: widget });
    }
  });
};

exports.embedhtml = function(req, res) {
  Widget.findById(req.params.id, function(err, widget) {
    if (err || widget === null) {
      res.json(404, "{error: not found}");
    } else {
      res.json(200, {title: widget.title, buttonBody: widget.buttonBody, formBody: widget.formBody, onSuccessBody: widget.onSuccessBody, onFailureBody: widget.onFailureBody});
    }
  });
};

