/*
 * GET users listing.
 */

// curl -v -H "Content-Type: application/json" -X GET -d '{}' http://localhost:3000/api/users
// curl -v -H "Content-Type: application/json" -X POST -d '{"user": {"username": "mhuneke21@gmail.com", "fullname": "Mike Huneke", "password": "password"}}' http://localhost:3000/api/users
// curl -v -H "Content-Type: application/json" -X PUT -d '{"user": {"username": "mhuneke18@gmail.com", "password": "password"}}' http://localhost:3000/api/users/505d2ab95996e041e3000001
// curl -v -H "Content-Type: application/json" -X DELETE -d '{}' http://localhost:3000/api/users/505d2ab95996e041e3000001

var async = require('async'),
    User = require('../../model/user'),
    Widget = require('../../model/widget'),
    WidgetCharge = require('../../model/widget-charge'),
    WidgetEvent = require('../../model/widget-event'),
    _ = require('underscore'),
    auth = require('../auth'),
    app = require('../../app');

exports.list = function(req, res) {
  var skip = req.query.skip || 0;
  var limit = app.API_PAGE_SIZE;

  async.waterfall([
    function(callback) {
      User.find().skip(skip).limit(limit).exec(callback);
    },
    function(users, callback) {
      User.count(function(err, total) {
        callback(users, total);
      });
    }
  ], function(users, total) {
    var result = {};
    result.data = users;
    result.page = (skip / limit) + 1;
    result.perpage = limit;
    result.total = total;
    res.json(result);
  });
};

exports.create = function(req, res) {
  User.create(req.body.user, function(err, user) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, user);
    }
  });
};

exports.get = function(req, res) {
  var me = {
    fullname: req.session.currentUser.fullname,
    checkingAccountNumber: req.session.currentUser.checkingAccountNumber,
    checkingAccountRoutingNumber: req.session.currentUser.checkingAccountRoutingNumber,
    mailchimpAPIKey: req.session.currentUser.mailchimpAPIKey
  }
  res.json(200, me);
};

exports.update = function(req, res) {
  User.findByIdAndUpdate(req.params.id, req.body.user, function(err, user) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, {"success": "true"});
    }
  });
};

exports.updateCurrentUser = function(req, res) {
  User.findByIdAndUpdate(req.session.currentUser._id, req.body, function(err, user) {
    if (err) {
      res.json(500, err);
    } else {
      req.session.currentUser = user;
      res.json(200, {"success": "true"});
    }
  });
};

exports.del = function(req, res) {
  User.findByIdAndRemove(req.params.id, function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, {"success": "true"});
    }
  });
};

exports.dashboard = function(req, res) {
  var dashboard = {};
  async.waterfall([
    function(callback) {
      User.findById(req.session.currentUser._id, function(err, user) {
        dashboard.amountOwed = user.amountOwed;
        callback(err, user);
      });
    },
    function(user, callback) {
      Widget.find({user: req.session.currentUser._id}, 'id', callback);
    },
    function(widgets, callback) {
      var widgetIds = _.map(widgets, function(widget) { return widget._id; });
      dashboard.widgetCount = widgets.length;
      WidgetEvent.count({type: 'loaded'}).where('widget').in(widgetIds).exec(function(err, widgetEventCount) {
        callback(err, widgetIds, widgetEventCount);
      });
    },
    function(widgetIds, widgetEventCount, callback) {
      WidgetCharge.find().where('widget').in(widgetIds).select('amount').exec(function(err, charges) {
        dashboard.totalMoneyRaised = _.reduce(charges, function(memo, charge){ return memo + charge.amount; }, 0);
        dashboard.conversionPercentage = widgetEventCount === 0 ? 0 : ((charges.length / widgetEventCount) * 100).toFixed(2);
        callback(err, dashboard);
      });
    }
  ], function(err, dashboard) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(200, dashboard);
    }
  });
};