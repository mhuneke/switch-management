/*
 * mailchimp
 */

// curl -v -H "Content-Type: application/json" -X GET -d '{}' http://localhost:3000/api/mailchimp/lists

var MailChimpAPI = require('mailchimp').MailChimpAPI;

exports.lists = function(req, res) {
  if (!req.session.currentUser.mailchimpAPIKey) {
    return res.json(404, {error: "No API key specified"});
  }

  var api = new MailChimpAPI(req.session.currentUser.mailchimpAPIKey, { version : '1.3', secure : false });

  api.lists(function(err, response) {
    res.json(200, response);
  });
};

exports.listGroupings = function(req, res) {
  if (!req.session.currentUser.mailchimpAPIKey) {
    return res.json(404, {error: "No API key specified"});
  }

  var api = new MailChimpAPI(req.session.currentUser.mailchimpAPIKey, { version : '1.3', secure : false });

  api.listInterestGroupings(req.params.id, function(err, groupings) {
    res.json(200, groupings);
  });
};


exports.subscribeToList = function(apiKey, listId, emailAddress, groupId) {
  var api = new MailChimpAPI(apiKey, { version : '1.3', secure : false });

  var merge_vars = {};
  if (groupId) {
    marge_vars.GROUPINGS = [{id: groupId, groups: 'donor'}];
  }
  api.listSubscribe({
    id: listId,
    email_address: emailAddress,
    double_optin: false,
    replace_interests: false,
    marge_vars: merge_vars
  }, function(err, response) {
    console.log(response);
  });
};
