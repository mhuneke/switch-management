// curl -v -X GET http://localhost:3000/api/wigets/50605a9610bb55b2cf000001/events/create?type=loaded
// curl -v -X GET http://localhost:3000/api/wigets/50605a9610bb55b2cf000001/events

var async = require('async'),
    WidgetEvent = require('../../model/widget-event'),
    Client = require('../../model/client'),
    app = require('../../app'),
    crypto = require('crypto');

exports.list = function(req, res) {
  var skip = req.query.skip || 0;
  var limit = app.API_PAGE_SIZE;
  // TODO: protect by currently logged in user

  async.waterfall([
    function(callback) {
      WidgetEvent.find({widget: req.params.id}).skip(skip).limit(limit).exec(callback);
    },
    function(widgetEvents, callback) {
      WidgetEvent.count({widget: req.params.id}, function(err, total) {
        callback(widgetEvents, total);
      });
    }
  ], function(widgetEvents, total) {
    var result = {};
    result.data = widgetEvents;
    result.page = (skip / limit) + 1;
    result.perpage = limit;
    result.total = total;
    res.json(result);
  });
};

exports.create = function(req, res) {
  if (req.params.id === null) {
    res.json(500, {error: "widget not found"});
  } else {
    async.waterfall([
      function(callback) {
        if (req.cookies.switchuser) {
          Client.findOne({username: req.cookies.switchuser}, callback);
        } else {
          Client.create({username: new Buffer(crypto.randomBytes(32)).toString('base64')}, callback);
        }
      }, function(client, callback) {
        var widgetEvent = {
          widget: req.params.id,
          type: req.query.type,
          client: client._id,
          userAgent: req.headers['User-Agent'],
          referer: req.headers['Referer'],
          remoteAddress: req.headers['HTTP_X_FORWARDED'] || req.connection.remoteAddress
        };
        WidgetEvent.create(widgetEvent);
        callback(null, client);
      }
    ], function(err, client) {
      res.cookie('switchuser', client.username, {expires: new Date(Date.now()+ app.COOKIE_EXPIRE_TIME), path: '/'});
      res.json(200, {success: true});
    });
  }
};
