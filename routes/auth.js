// curl -v -H "Content-Type: application/json" -X POST -d '{"user": {"username": "mhuneke15@gmail.com", "password": "password"}}' http://localhost:3000/api/users
// curl -v -d "username=mhuneke13@gmail.com&password=password&remember=on" http://localhost:3000/login
// curl -v -G -d "" http://localhost:3000/logout

var User = require('../model/user'),
    Token = require('../model/token'),
    app = require('../app');

exports.checkAuthentication = function(req, res, next){
  //checks to see if the user has logged in via a session
  if(req.session.currentUser) {
    next();
  } else if(req.cookies.logintoken) { //logs a user in if a token is present
    authenticateFromToken(req, res, next);
  } else {
    req.session.forward = req.originalUrl;
    return res.redirect('/login');
  }
};

exports.checkAPIAuthentication = function(req, res, next){
  //checks to see if the user has logged in via a session
  if(req.session.currentUser) {
    next();
  } else if(req.cookies.logintoken) { //logs a user in if a token is present
    authenticateFromToken(req, res, next);
  } else {
    res.json(403, "{error: unauthenticated}");
  }
};

var authenticateFromToken = function(req, res, next) {
  var cookie = JSON.parse(req.cookies.logintoken);
  //Check to see if the loginToken exists
  Token.findOne({username: cookie.username, series: cookie.series, token: cookie.token, app: 'web'}, function(err, loginToken) {
    //If not, the user has to log in again
    if(!loginToken) {
      next("token is blank");
    }
    //Now we load the user profiles as if we were logging in
    User.findOne({username: loginToken.username}, function(err, loadedUser){
      if (!loadedUser) {
        next("user is blank");
      }
      loginToken.generateToken();
      loginToken.save(function(err) {
        res.cookie('logintoken', JSON.stringify(loginToken.tokenValue), {expires: new Date(Date.now()+ app.COOKIE_EXPIRE_TIME), path: '/'});
        attachProfilesAndForward(loadedUser, req, res, next);
      });
    });
  });
};

exports.login = function(req, res) {
  if (!req.paramXss('username')) {
    return res.redirect('/login?loginfailed=1');
  }
  var username = req.paramXss('username').toLowerCase(), password = req.paramXss('password');
  User.findOne({username: username}, function(err, loadedUser) {
    if(err || loadedUser == null) {
      return res.redirect('/login?loginfailed=1');
    }
    if(loadedUser.authenticate(password)) {
      
      var loginSuccess = function() {
        var loginCount = (loadedUser.loginCount == null) ? 1 : loadedUser.loginCount + 1;
        User.findByIdAndUpdate(loadedUser.id, {lastLogin: new Date().getTime(), loginCount: loginCount})

        //Set req.session.currentUser and 
        //remove the password hash just to be safe
        req.session.currentUser = loadedUser;
        req.session.currentUser.hashed_password = '';
        req.session.currentUser.salt = '';

        if(req.session.forward) {
          var redirectStr = req.session.forward;
          req.session.forward = "";
          res.redirect(redirectStr);
        }
        res.redirect('/app/');
      };

      //Set the login token if remember me is checked
      if (req.paramXss('remember') == "on") {
        var loginToken = new Token({username: loadedUser.username, app: 'web'});
        loginToken.generateToken();
        loginToken.save(function(err) {
          res.cookie('logintoken', JSON.stringify(loginToken.tokenValue), {expires: new Date(Date.now()+ app.COOKIE_EXPIRE_TIME), path: '/'});
          return loginSuccess();
        });
      } else {
        return loginSuccess();
      }
    } else {
      res.redirect('/login?loginfailed=1');
    }
  });
};

exports.register = function(req, res) {
  User.create(req.body, function(err, user) {
    if (err) {
      console.log(err);
      res.redirect('/register?failed=1');
    } else {
      req.session.currentUser = user;
      req.session.currentUser.hashed_password = '';
      req.session.currentUser.salt = '';
      res.redirect('/app/');
    }
  });
  
};

exports.logout = function(req, res) {
  if(req.session.currentUser) {
    Token.remove({username: req.session.currentUser.username, app: 'web'}, function(err, removedTokens) {
      res.clearCookie('logintoken');
      req.session.destroy(function() {
        res.redirect('/');
      });
    });
  } else {
    req.session.destroy(function() {
      res.redirect('/');
    });
  }
};

exports.failedLogin = function (req, res) {
  //Set req.session.forward
  req.session.forward = req.originalUrl;
  //Forward to the login page
  res.redirect('/login');
};

exports.loginview = function(req, res) {
  res.render('static/login');
};

exports.registerview = function(req, res) {
  res.render('static/register');
};