module.exports = exports = function lastModifiedPlugin(schema, options){
  
  schema.add({dateModified: Date});

  schema.pre('save', function(next){
    this.dateModified = new Date().getTime();
    next();
  });

  if(options && options.index) {
    schema.path('dateModified').index(options.index);   
  }
};
