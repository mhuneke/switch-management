var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

var tokenSchema = new Schema({
  username: {type: String, index: true},
  series: {type: String, index: true},
  token: {type: String, index: true},
  app: {type: String, enum: ['mobile', 'web'], required: true},
  lastRequest: {type: String},
  lastResponse: {type: String}
});

tokenSchema.methods.generateToken = function generateToken() {
  //Auto-create the tokens
  this.token = this.randomToken();
  if(!this.series){
    this.series = this.randomToken();
  }
}

tokenSchema.methods.randomToken = function randomToken(){
  return Math.round((new Date().valueOf() * Math.random())) + '';
};

tokenSchema.virtual('id').get(function(){
  return this._id.toHexString();
});

tokenSchema.virtual('tokenValue').get(function(){
  return {username: this.username, token: this.token, series: this.series};
});

mongoose.model('Token', tokenSchema);
module.exports = mongoose.model('Token');
