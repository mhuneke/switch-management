var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified');

var widgetEventSchema = new Schema({
  widget: {type: ObjectId, ref: 'Widget', required: true},
  client: {type: ObjectId, ref: 'Client', required: true},
  type: String,
  dateCreated: Date,
  userAgent: String,
  referer: String,
  remoteAddress: String
});

widgetEventSchema.pre('save', function(next) {
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  next();
});

widgetEventSchema.plugin(modify);

mongoose.model('WidgetEvent', widgetEventSchema);
module.exports = mongoose.model('WidgetEvent');
