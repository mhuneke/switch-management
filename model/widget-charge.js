var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified');

var widgetChargeSchema = new Schema({
  widget: {type: ObjectId, ref: 'Widget', required: true},
  client: {type: ObjectId, ref: 'Client', required: true},
  amount: Number,
  dateCreated: Date,
  fee: Number,
  feeDetails: String,
  externalTransactionId: String
});

widgetChargeSchema.pre('save', function(next) {
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  next();
});

widgetChargeSchema.plugin(modify);

mongoose.model('WidgetCharge', widgetChargeSchema);
module.exports = mongoose.model('WidgetCharge');
