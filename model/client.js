var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified'),
  crypto = require('crypto');

var clientSchema = new Schema({
  username: {type: String, unique: true, required: true}
});

clientSchema.pre('save', function(next) {
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  next();
});

clientSchema.plugin(modify);

mongoose.model('Client', clientSchema);
module.exports = mongoose.model('Client');
