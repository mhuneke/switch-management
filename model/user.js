var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified'),
  crypto = require('crypto');

var userSchema = new Schema({
  username: {type: String, unique: true, required: true},
  fullname: String,
  organization: String,
  hashed_password: {type: String},                    //hash only 
  salt : {type: String, default: 'changeme'},                   //salt used to create hash
  active: {type: Boolean},
  dateCreated: {type: Date},
  loginCount: {type: Number, default: 0},
  lastLogin: Date,
  checkingAccountNumber: Number,
  checkingAccountRoutingNumber: Number,
  amountOwed: {type: Number, default: 0},
  mailchimpAPIKey: String
});

userSchema.methods.encryptPassword = function encryptPassword(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

userSchema.methods.makeSalt = function makeSalt() {
  return new Buffer(crypto.randomBytes(4)).toString('base64');
};

userSchema.methods.authenticate = function authenticate(plainText) {
  return this.encryptPassword(plainText) === this.hashed_password;
};

userSchema.pre('save', function(next) {
  try {
    check(this.username, "Invalid username").len(4,50).isEmail();
  } catch(err) {
    next(new Error(err));
    return;
  }
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  this.username = this.username.toLowerCase();
  next();
});

userSchema.virtual('password').set(function(password) {
  if(password.length >= 6 && password.length < 50) {
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  }
});

userSchema.plugin(modify);

mongoose.model('User', userSchema);
module.exports = mongoose.model('User');
