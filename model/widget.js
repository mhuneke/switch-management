var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified');

var widgetSchema = new Schema({
  projectTitle: String,
  user: {type: ObjectId, ref: 'User', required: true},
  title: String,
  width: Number,
  height: Number,
  webColorCode: String,
  formBody: String,
  buttonBody: String,
  onSuccessBody: String,
  onFailureBody: String,
  socialEnabled: Boolean,
  dateCreated: Date,
  viewCount: {type: Number, default: 0},
  donorCount: {type: Number, default: 0},
  totalMoneyRaised: {type: Number, default: 0},
  subscribeMailchimpId: String,
  subscribeMailchimpName: String,
  subscribeMailchimpGroupingId: String,
  subscribeMailchimpGroupingName: String
});

widgetSchema.pre('save', function(next) {
  try {

  } catch(err) {
    next(new Error(err));
    return;
  }
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  next();
});

widgetSchema.plugin(modify);

mongoose.model('Widget', widgetSchema);
module.exports = mongoose.model('Widget');
