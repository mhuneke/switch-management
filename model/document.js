var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId,
  check = require('validator').check,
  modify = require('./plugins/last_modified');

var documentSchema = new Schema({
  user: {type: ObjectId, ref: 'User', required: true},
  name: String,
  path: String
});

documentSchema.pre('save', function(next) {
  try {

  } catch(err) {
    next(new Error(err));
    return;
  }
  if(this.isNew) {
    this.dateCreated = new Date().getTime();
  }
  next();
});

documentSchema.plugin(modify);

mongoose.model('Document', documentSchema);
module.exports = mongoose.model('Document');
